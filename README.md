# COVID DataDAM #

COVID DataDAM is a digital asset management tool for datasets relating to COVID-19.

### What is COVID DataDAM? ###

DataDAM is a digital asset management tool for datasets that stores, shares, and collates assets in a central location.
These assets include curated datasets and ML models, notebooks, visualizations, documents, and videos.
COVID DataDAM is a tool specifically for COVID-19.

### What does COVID DataDAM do? ###

COVID DataDAM provides storage, security, organization, and searchability for COVID-19 assets.
It automates workflows and manages press kits and picture collections.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact